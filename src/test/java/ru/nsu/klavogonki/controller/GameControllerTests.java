package ru.nsu.klavogonki.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;
import ru.nsu.klavogonki.FunctionalTest;

class GameControllerTests extends FunctionalTest {
    @Test
    void getLangs() throws Exception {
        mock.perform(MockMvcRequestBuilders.get("/game/langs"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(
                        json.from(ResourceUtils.getFile(
                                "classpath:gameController/langs.json"
                        )).getJson()
                ));
    }

    @Test
    void getText() throws Exception {
        int count = 20;
        String lang = "russian";
        MvcResult result = mock.perform(MockMvcRequestBuilders.get(
                String.format("/game/text?count=%d&lang=%s", count, lang)
                )).andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        Assertions.assertEquals(result.getResponse().getContentAsString().split(" ").length, count);
    }
}
