package ru.nsu.klavogonki.controller;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.ResourceUtils;
import ru.nsu.klavogonki.FunctionalTest;
import ru.nsu.klavogonki.model.stats.StatsDto;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class StatsControllerTests extends FunctionalTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @DisplayName("Тест что ручка работает и репозиторий корректно кладет данные в базу.")
    @DataSet("statsController/statsRepositoryOneUserWithoutRes.xml")
    void putStatsTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String putStatsJson = json.from(ResourceUtils.getFile(
                "classpath:statsController/putStats.json"
        )).getJson();

        mock.perform(MockMvcRequestBuilders
                .put("/game/stats/sendResult").contentType(MediaType.APPLICATION_JSON)
                .content(putStatsJson)
        );

        final StatsDto expectedStatsDto = objectMapper.readValue(putStatsJson, StatsDto.class);

        List<StatsDto> statsDtos =
                jdbcTemplate.query("select * from stats", (resultSet, i) ->
                        StatsDto.builder()
                                .username(resultSet.getObject("username", String.class))
                                .speed(resultSet.getObject("typing_speed", Float.class))
                                .timestamp(resultSet.getObject("date_time", Timestamp.class))
                                .build()
                );

        assertThat(statsDtos, contains(expectedStatsDto));
    }

    @Test
    @DisplayName("Топ результатов корректно отдается.")
    @DataSet("statsController/statsRepositoryTwoUsers.xml")
    void getStatsTopTest() throws Exception {
        mock.perform(MockMvcRequestBuilders
                        .get("/game/stats/getTop").contentType(MediaType.APPLICATION_JSON)
                        .param("maxRows", "10")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].username", is("vasya")))
                .andExpect(jsonPath("$[0].speed", is(80.0)))
                .andExpect(jsonPath("$[1].username", is("admin")))
                .andExpect(jsonPath("$[1].speed", is(60.0)));
    }

    @Test
    @DisplayName("Результаты для юзера отдаются корректно.")
    @DataSet("statsController/statsRepositoryTwoUsers.xml")
    void getStatsByUserTest() throws Exception {
        mock.perform(MockMvcRequestBuilders
                        .get("/game/stats/getAllByUser").contentType(MediaType.APPLICATION_JSON)
                        .param("username", "vasya")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].username", is("vasya")))
                .andExpect(jsonPath("$[0].speed", is(80.0)))
                .andExpect(jsonPath("$[1].username", is("vasya")))
                .andExpect(jsonPath("$[1].speed", is(30.0)));
    }
}
