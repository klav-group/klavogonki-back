package ru.nsu.klavogonki.controller;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;
import ru.nsu.klavogonki.FunctionalTest;
import ru.nsu.klavogonki.service.AccountService;

class AccountTests extends FunctionalTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountService accountService;

    @BeforeEach
    public void clearAccountTable() {
        jdbcTemplate.execute("delete from stats; delete from account");
    }

    @Test
    void testCreateAndGetPerson() throws Exception {
        mock.perform(MockMvcRequestBuilders
                .post("/account").contentType(MediaType.APPLICATION_JSON).content(
                        json.from(ResourceUtils.getFile(
                                "classpath:accountController/putAdminUser.json"
                        )).getJson()
                )
        );

        mock.perform(MockMvcRequestBuilders.get("/account/admin"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(json.from(ResourceUtils.getFile(
                                "classpath:accountController/getAdminUser.json"
                        )).getJson()
                ));
    }

    @Test
    void testNotFound() throws Exception {
        mock.perform(MockMvcRequestBuilders.get("/account/admin"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void testGetByUsernameNotExist() {
        try {
            accountService.loadUserByUsername("admin");
            Assertions.fail();
        } catch (UsernameNotFoundException ignored) {
        }
    }

    @Test
    void testGetByUsername() throws Exception {
        mock.perform(MockMvcRequestBuilders
                .post("/account").contentType(MediaType.APPLICATION_JSON).content(
                        json.from(ResourceUtils.getFile(
                                "classpath:accountController/putAdminUser.json"
                        )).getJson()
                )
        );

        UserDetails userDetails = accountService.loadUserByUsername("admin");
        Assertions.assertEquals("admin", userDetails.getUsername());
        Assertions.assertEquals(Set.of(), userDetails.getAuthorities());
    }

    @Test
    void testConflictError() throws Exception {
        mock.perform(MockMvcRequestBuilders
                .post("/account").contentType(MediaType.APPLICATION_JSON).content(
                        json.from(ResourceUtils.getFile(
                                "classpath:accountController/putAdminUser.json"
                        )).getJson()
                )
        ).andExpect(MockMvcResultMatchers.status().isOk());

        mock.perform(MockMvcRequestBuilders
                .post("/account").contentType(MediaType.APPLICATION_JSON).content(
                        json.from(ResourceUtils.getFile(
                                "classpath:accountController/putAdminUser.json"
                        )).getJson()
                )
        ).andExpect(MockMvcResultMatchers.status().isConflict());
    }
}


