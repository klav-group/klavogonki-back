package ru.nsu.klavogonki.repository;

import java.sql.Timestamp;
import java.util.List;

import com.github.database.rider.core.api.dataset.DataSet;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.nsu.klavogonki.FunctionalTest;
import ru.nsu.klavogonki.model.stats.StatsDto;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasProperty;

class StatsRepositoryTest extends FunctionalTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private StatsRepository statsRepository;

    @Test
    @DisplayName("Тест что репозиторий корректно кладет данные в базу.")
    @DataSet("statsController/statsRepositoryOneUser.xml")
    void sendStatsTest() throws Exception {
        List<StatsDto> statsDtos =
                jdbcTemplate.query("select * from stats", (resultSet, i) ->
                        StatsDto.builder()
                                .username(resultSet.getObject("username", String.class))
                                .speed(resultSet.getObject("typing_speed", Float.class))
                                .timestamp(resultSet.getObject("date_time", Timestamp.class))
                                .build()
                );

        assertThat(statsDtos, contains(allOf(
                hasProperty(
                        "username", equalTo("admin")),
                hasProperty(
                        "speed", equalTo(20.5))
        )));
    }

    @Test
    @DisplayName("Топ результатов корректно отдается.")
    @DataSet("statsController/statsRepositoryTwoUsers.xml")
    void getStatsTopTest() {
        List<StatsDto> result = statsRepository.getTopResults(10);
        assertThat(result.get(0).getUsername(), equalTo("vasya"));
        assertThat(result.get(0).getSpeed(), equalTo(80.0));
        assertThat(result.get(1).getUsername(), equalTo("admin"));
        assertThat(result.get(1).getSpeed(), equalTo(60.0));
    }

    @Test
    @DisplayName("Результаты для юзера отдаются корректно.")
    @DataSet("statsController/statsRepositoryTwoUsers.xml")
    void getStatsByUserTest() {
        List<StatsDto> result = statsRepository.getAllByUser("admin");
        assertThat(result, everyItem(hasProperty("username", equalTo("admin"))));
        assertThat(result, containsInAnyOrder(
                hasProperty("speed", equalTo(50.0)),
                hasProperty("speed", equalTo(60.0))));
    }
}
