package ru.nsu.klavogonki.service;

import java.util.List;

import javax.ws.rs.BadRequestException;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.klavogonki.model.account.Account;
import ru.nsu.klavogonki.model.account.AccountDto;
import ru.nsu.klavogonki.model.account.AccountInputDto;
import ru.nsu.klavogonki.repository.AccountRepository;

@Service
@RequiredArgsConstructor
public class AccountService implements UserDetailsService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    public void saveUser(AccountInputDto userDao) {
        try {
            Account account = Account.builder()
                    .username(userDao.getUsername())
                    .passwordHash(passwordEncoder.encode(userDao.getPassword()))
                    .email(userDao.getEmail())
                    .build();

            accountRepository.save(account);
        } catch (IllegalArgumentException err) {
            throw new BadRequestException("Wrong username credentials");
        }
    }

    public AccountDto getUserDto(String username) {
        Account account = accountRepository.findByUsername(username);

        if (account == null) {
            return null;
        }

        return AccountDto.builder()
                .email(account.getEmail())
                .username(account.getUsername())
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException(username + " doesn't exists");
        }
        return new User(account.getUsername(), account.getPasswordHash(), List.of()); //todo add roles
    }
}
