package ru.nsu.klavogonki.service;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.klavogonki.model.stats.StatsDto;
import ru.nsu.klavogonki.repository.StatsRepository;

@Service
@AllArgsConstructor
public class StatsService {
    private StatsRepository statsRepository;

    public void sendResult(StatsDto statsDto) {
        statsRepository.sendResult(statsDto);
    }

    public List<StatsDto> getAllByUser(String username) {
        return statsRepository.getAllByUser(username);
    }

    public List<StatsDto> getTopResults(int cnt) {
        return statsRepository.getTopResults(cnt);
    }
}
