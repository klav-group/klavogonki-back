package ru.nsu.klavogonki.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.klavogonki.model.text.TextEntity;
import ru.nsu.klavogonki.model.text.TextModel;
import ru.nsu.klavogonki.model.text.UserTextEntity;
import ru.nsu.klavogonki.model.text.UserTextModel;
import ru.nsu.klavogonki.repository.TextRepository;
import ru.nsu.klavogonki.repository.UserTextsRepository;

@Service
public class TextService {
    private final TextRepository textRepository;
    private final UserTextsRepository userTextsRepository;
    private final Random random;

    public TextService(
        @Autowired TextRepository textRepository,
        @Autowired UserTextsRepository userTextsRepository
    ) throws NoSuchAlgorithmException {
        this.textRepository = textRepository;
        this.userTextsRepository = userTextsRepository;
        this.random = SecureRandom.getInstanceStrong();
    }

    public TextModel generateText(@NonNull String language, Integer count) {
        int cnt = count == null ? 5 : count;
        List<TextEntity> textEntities = textRepository.findAllByLanguage(language);
        TextEntity textEntity = textEntities.get(random.nextInt(textEntities.size()));
        List<String> dictionary = Arrays.stream(textEntity.getText().split(","))
            .collect(Collectors.toList());
        List<String> text = new LinkedList<>();
        for (int i = 0; i < cnt; i++) {
            text.add(dictionary.get(random.nextInt(dictionary.size())));
        }
        return new TextModel(StringUtils.join(text, ' '));
    }

    public List<String> getLanguages() {
        return textRepository.getAllDistinctLanguages();
    }

    public List<UserTextModel> getAllTextsByUser(String username) {
        return userTextsRepository.getAllByUsername(username).stream()
            .map(userTextEntity ->
                new UserTextModel(
                    userTextEntity.getUsername(),
                    userTextEntity.getName(),
                    userTextEntity.getText()
                ))
            .collect(Collectors.toList());
    }

    @Nullable
    public UserTextModel getTextByUsernameAndName(String username, String name) {
        UserTextEntity entity = userTextsRepository
            .getByUsernameAndName(username, name).orElse(null);
        if (entity == null) {
            return null;
        }
        return new UserTextModel(entity.getUsername(), entity.getName(), entity.getText());
    }

    public void saveUserText(UserTextModel textModel) {
        userTextsRepository.save(
            new UserTextEntity(textModel.getUsername(), textModel.getName(), textModel.getText())
        );
    }
}
