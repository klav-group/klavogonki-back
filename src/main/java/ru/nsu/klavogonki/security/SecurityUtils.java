package ru.nsu.klavogonki.security;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class SecurityUtils {
    private SecurityUtils() {
        
    }

    public static String generateAccessToken(String username, HttpServletRequest request) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles", List.of()) //todo roles
                .sign(getAlgo());
    }

    public static Cookie generateRefreshCookie(String username, HttpServletRequest request) {
        String refreshToken = JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000))
                .withIssuer(request.getRequestURL().toString())
                .sign(getAlgo());

        Cookie cookie = new Cookie("refresh_token", refreshToken);
        cookie.setSecure(true);
        cookie.setPath("/");
        cookie.setMaxAge(24 * 60 * 60 * 1000);

        return cookie;
    }

    public static Algorithm getAlgo() {
        return Algorithm.HMAC256("secretToken".getBytes(StandardCharsets.UTF_8));
    }
}
