package ru.nsu.klavogonki.security.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.nsu.klavogonki.model.account.AccountInputDto;
import ru.nsu.klavogonki.security.SecurityUtils;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request, HttpServletResponse response,
            FilterChain chain, Authentication authResult) throws IOException {
        User user = (User) authResult.getPrincipal();
        Map<String, String> tokens = new HashMap<>();
        tokens.put("token", SecurityUtils.generateAccessToken(user.getUsername(), request));
        tokens.put("username", user.getUsername());
        response.addCookie(SecurityUtils.generateRefreshCookie(user.getUsername(), request));
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getWriter(), tokens);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Gson gson = new Gson();
        try {
            AccountInputDto inputDto = gson.fromJson(request.getReader(), AccountInputDto.class);
            String username = inputDto.getUsername();
            String password = inputDto.getPassword();
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(username, password);
            return authenticationManager.authenticate(authenticationToken);
        } catch (IOException | NullPointerException e) {
            //todo log
            throw new AuthenticationCredentialsNotFoundException("Username and password not found");
        }
    }
}
