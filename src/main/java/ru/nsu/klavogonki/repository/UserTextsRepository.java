package ru.nsu.klavogonki.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.klavogonki.model.text.UserTextEntity;

@Repository
public interface UserTextsRepository extends JpaRepository<UserTextEntity, Long> {
    List<UserTextEntity> getAllByUsername(String username);

    Optional<UserTextEntity> getByUsernameAndName(String username, String name);
}
