package ru.nsu.klavogonki.repository;

import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.nsu.klavogonki.model.stats.StatsDto;

@Repository
@AllArgsConstructor
public class StatsRepository {

    private static final String PUT_RESULT_QUERY =
            "INSERT INTO stats (username, typing_speed, date_time) VALUES (?, ?, ?)";

    private static final String GET_TOP_RESULTS =
            "with mtst as (\n" +
                    "select username, max(typing_speed) mts\n" +
                    "from stats\n" +
                    "group by username)\n" +
                    "select mtst.username, mtst.mts, t.date_time\n" +
                    "from stats t, mtst\n" +
                    "where t.username = mtst.username and t.typing_speed = mtst.mts\n" +
                    "order by mtst.mts desc\n" +
                    "fetch first ? rows only; ";

    private static final String GET_ALL_BY_USER =
            "select * " +
                    "from stats " +
                    "where username = ? " +
                    "order by typing_speed desc ";


    private JdbcTemplate jdbcTemplate;

    public void sendResult(String username, double typingSpeed, Timestamp timestamp) {
        jdbcTemplate.update(PUT_RESULT_QUERY, username, typingSpeed, timestamp);
    }

    public void sendResult(StatsDto statsDto) {
        sendResult(statsDto.getUsername(), statsDto.getSpeed(), statsDto.getTimestamp());
    }

    public List<StatsDto> getAllByUser(String username) {
        return jdbcTemplate.query(GET_ALL_BY_USER, (resultSet, i) ->
                        StatsDto.builder()
                                .username(resultSet.getObject("username", String.class))
                                .speed(resultSet.getObject("typing_speed", Double.class))
                                .timestamp(resultSet.getObject("date_time", Timestamp.class))
                                .build(),
                username
        );
    }

    public List<StatsDto> getTopResults(int cnt) {
        return jdbcTemplate.query(GET_TOP_RESULTS, (resultSet, i) ->
                        StatsDto.builder()
                                .username(resultSet.getObject("username", String.class))
                                .speed(resultSet.getObject("mts", Double.class))
                                .timestamp(resultSet.getObject("date_time", Timestamp.class))
                                .build(),
                cnt
        );
    }
}
