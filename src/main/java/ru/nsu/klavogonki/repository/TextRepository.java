package ru.nsu.klavogonki.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.klavogonki.model.text.TextEntity;

@Repository
public interface TextRepository extends JpaRepository<TextEntity, Long> {
    @Query(value = "SELECT DISTINCT a.language FROM TextEntity a ")
    List<String> getAllDistinctLanguages();

    List<TextEntity> findAllByLanguage(String language);
}
