package ru.nsu.klavogonki.controller;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.nsu.klavogonki.model.account.AccountDto;
import ru.nsu.klavogonki.model.account.AccountInputDto;
import ru.nsu.klavogonki.service.AccountService;

@RestController
@AllArgsConstructor
public class AccountController {

    private AccountService accountService;

    @PostMapping("/account")
    public void createAccount(@RequestBody AccountInputDto user) {
        try {
            accountService.saveUser(user);
        } catch (DataIntegrityViolationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exists");
        }
    }

    @GetMapping("/account/{username}")
    public ResponseEntity<AccountDto> getAccount(@PathVariable @Nonnull String username) {

        @Nullable
        AccountDto user = accountService.getUserDto(username);

        if (user == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
