package ru.nsu.klavogonki.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.klavogonki.model.text.TextModel;
import ru.nsu.klavogonki.model.text.UserTextModel;
import ru.nsu.klavogonki.service.TextService;

@RestController
@RequiredArgsConstructor
public class GameController {
    private final TextService textService;

    @GetMapping("/game/text")
    public ResponseEntity<TextModel> getText(
            @RequestParam(required = false) Integer count,
            @RequestParam String lang
    ) {
        return new ResponseEntity<>(textService.generateText(lang, count), HttpStatus.OK);
    }

    @GetMapping("/game/langs")
    public ResponseEntity<List<String>> getLanguages() {
        return new ResponseEntity<>(textService.getLanguages(), HttpStatus.OK);
    }

    @GetMapping("/game/allusertexts")
    public ResponseEntity<List<UserTextModel>> getAllUserTexts(@RequestParam String username) {
        return new ResponseEntity<>(textService.getAllTextsByUser(username), HttpStatus.OK);
    }

    @GetMapping("/game/usertext")
    public ResponseEntity<UserTextModel> getUserText(
        @RequestParam String username,
        @RequestParam String name
    ) {
        return new ResponseEntity<>(
            textService.getTextByUsernameAndName(username, name),
            HttpStatus.OK);
    }

    @PutMapping("/game/usertext")
    public void saveUserText(@RequestBody UserTextModel textModel) {
        textService.saveUserText(textModel);
    }
}
