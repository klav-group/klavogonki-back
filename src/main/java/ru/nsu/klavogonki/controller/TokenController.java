package ru.nsu.klavogonki.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.BadRequestException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import ru.nsu.klavogonki.model.account.AccountDto;
import ru.nsu.klavogonki.security.SecurityUtils;
import ru.nsu.klavogonki.service.AccountService;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class TokenController {
    private AccountService accountService;

    @GetMapping("/token/username")
    public String getAccount(HttpServletRequest request) {
        return request.getUserPrincipal().getName();
    }

    @GetMapping("/refresh_token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String refreshToken = Arrays.stream(request.getCookies() != null ? request.getCookies() : new Cookie[0])
                .filter(cookie -> cookie.getName().equals("refresh_token"))
                .findAny()
                .map(Cookie::getValue)
                .orElse(null);

        if (refreshToken != null) {
            try {
                JWTVerifier verifier = JWT.require(SecurityUtils.getAlgo()).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String username = decodedJWT.getSubject();
                AccountDto account = accountService.getUserDto(username);

                Map<String, String> tokens = new HashMap<>();
                tokens.put("token", SecurityUtils.generateAccessToken(username, request));
                tokens.put("username", account.getUsername());
                response.setContentType(APPLICATION_JSON_VALUE);
                response.addCookie(SecurityUtils.generateRefreshCookie(username, request));
                new ObjectMapper().writeValue(response.getWriter(), tokens);
            } catch (Exception exception) {
                //todo LOG
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new BadRequestException("Refresh token is missing");
        }
    }
}
