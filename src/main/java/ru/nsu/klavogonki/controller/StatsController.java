package ru.nsu.klavogonki.controller;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.klavogonki.model.stats.StatsDto;
import ru.nsu.klavogonki.service.StatsService;

@RestController
@AllArgsConstructor
public class StatsController {

    private StatsService statsService;

    @PutMapping("/game/stats/sendResult")
    public void sendResult(@RequestBody StatsDto statsDto) {
        statsService.sendResult(statsDto);
    }

    @GetMapping("/game/stats/getTop")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<StatsDto>> getTop(@RequestParam int maxRows) {
        return new ResponseEntity<>(
                statsService.getTopResults(maxRows),
                HttpStatus.OK);
    }

    @GetMapping("/game/stats/getAllByUser")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<StatsDto>> getAllByUser(@RequestParam String username) {
        return new ResponseEntity<>(
                statsService.getAllByUser(username),
                HttpStatus.OK);
    }
}
