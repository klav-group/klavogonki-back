package ru.nsu.klavogonki.model.account;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountDto {
    private String username;
    private String email;
}
