package ru.nsu.klavogonki.model.account;

import lombok.Data;

@Data
public class AccountInputDto {
    private String username;

    private String email;

    private String password;
}
