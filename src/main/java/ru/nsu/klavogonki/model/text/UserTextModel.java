package ru.nsu.klavogonki.model.text;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTextModel {
    @NonNull
    private String username;

    @NonNull
    private String name;

    @NonNull
    private String text;
}
