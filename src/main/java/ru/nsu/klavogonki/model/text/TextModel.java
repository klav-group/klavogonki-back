package ru.nsu.klavogonki.model.text;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextModel {
    private String text = "Hello";
}
