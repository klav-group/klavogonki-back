--liquibase formatted sql
--changeset ayakovlev:KLAV-77-added-texts-table
create table if not exists texts
(
    id       bigserial,
    language varchar(255),
    text   text,
    primary key (id)
);
