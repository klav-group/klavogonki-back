--liquibase formatted sql
--changeset ayakovlev:KLAV-82-added-usertexts-table
create table if not exists usertexts
(
    id       bigserial,
    username varchar(255),
    name     varchar(255),
    text     text,
    primary key (id),
    unique (username, name)
);
