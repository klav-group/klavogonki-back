--liquibase formatted sql
--changeset dzaikov:KLAV-66-added-stats-table
create table if not exists stats (
    username varchar(255),
    typing_speed float,
    date_time timestamp,
    foreign key (username) references account(username)
);