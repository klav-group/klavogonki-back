--liquibase formatted sql
--changeset yakovlev:KLAV-63-added-account-table
create table account
(
    id       bigserial,
    username varchar(255),
    email    varchar(255),
    password_hash text,
    primary key (id),
    unique (username),
    unique (email)
)
