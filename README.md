[![coverage report](https://gitlab.com/klav-group/klavogonki-back/badges/master/coverage.svg)](https://gitlab.com/klav-group/klavogonki-back/-/commits/master)

### Локальный запуск  
Для локального запуска Spring Boot run configuration. 

Main-class: ru.nsu.klavogonki.DemoApplication \
VM-options: -Dspring.profiles.active=test

 
